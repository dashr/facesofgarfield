<?php

/**
*  class.
*
*
*/
class Instatag {

  /**
  * get photos with more than 0 votes
  *
  * @access public
  * @static
  * @param int $json (default: 0)
  * @param int $offset (default: 0)
  * @param int $num (default: 0)
  * @return void
  */

  static public function get($json = 0, $offset = 0, $num = 0, $sort = "ASC", $votes = 0)
  {   
    $sql = sprintf("SELECT * 
      FROM media m 
      LEFT JOIN votes v ON m.gram_id=v.gram_id 
      WHERE v.votes >= %d 
      ORDER BY m.created_time %s
      LIMIT %d, %d", 
      $votes, $sort, $offset, $num
    );

    $data = self::get_all( $sql );

    if ( $json == 1)
    {
      header('Content-type: application/json');
      print json_encode( $data );
    }
    else
    {
      return $data;
    }
  }

  static public function vote($id = null, $vote)
  { 
    if(!$id) return false;
    if($vote !== -1 && $vote !== 1) return false;

    $sql = sprintf("UPDATE votes SET votes=votes+%d WHERE gram_id='%s' LIMIT 1", $vote, $id);

    global $dbh;
    $res = $dbh->query($sql);

    //get new score
    $new = sprintf("SELECT votes FROM votes WHERE gram_id='%s' LIMIT 1", $id);
    $votes = self::get_all($new);

    return $votes[0]['votes'];
  }

  /**
  * get_all function.
  *
  * @access public
  * @static
  * @param mixed $sql
  * @return void
  */
  static function get_all($sql)
  {
    global $dbh;
    $res = $dbh->query($sql);
    if ($res)
      return $res->fetchAll(PDO::FETCH_ASSOC);
  }


  /**
  * save images locally for Canvas manipulation, run on cron
  * @access public
  * @static
  * @param string $url
  * @return void
  */
  static public function cache($url)
  {
    $u = parse_url( $url );
    if ( ! file_exists( CACHE . $u['path'] ) )
    { 
      copy( $url, CACHE . $u['path'] );
    }
  }

}
