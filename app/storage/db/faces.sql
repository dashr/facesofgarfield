-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2013 at 06:37 AM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `faces`
--

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `gram_id` varchar(32) NOT NULL,
  `created_time` int(10) NOT NULL,
  `location` varchar(255) NOT NULL,
  `standard_url` varchar(255) NOT NULL,
  `hires_video_url` varchar(255) NOT NULL,
  `user_profile_picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `username` varchar(128) NOT NULL,
  `full_name` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`gram_id`),
  KEY `created_time` (`created_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`gram_id`, `created_time`, `location`, `standard_url`, `hires_video_url`, `user_profile_picture`, `link`, `caption`, `username`, `full_name`, `user_id`) VALUES
('579318213977869234_637076057', 1383280134, 'Garfield Historic District', 'http://distilleryimage10.s3.amazonaws.com/1de1f47242ae11e3b4c422000aeb2179_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gKJtikEIuy/', 'Rosendo Sandoval, serving ice cream in the #garfield #district for over 25 years! #facesofgarfield', 'facesofgarfield', 'Faces of Garfield', 637076057),
('579321347399781345_637076057', 1383280507, 'Garfield Historic District', 'http://distilleryimage9.s3.amazonaws.com/fc85ef0842ae11e3a4a222000ab50128_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gKKbIykIvh/', 'Ruiz family, residents of the #garfielddistrict for 12 years.  #halloween #facesofgarfield', 'facesofgarfield', 'Faces of Garfield', 637076057),
('579328188359936100_637076057', 1383281323, 'Garfield Historic District', 'http://distilleryimage0.s3.amazonaws.com/e29a166c42b011e3a4d822000a1f924b_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gKL-r7kIhk/', 'Nayelli, 2 years old, her family has been living in the #garfielddistrict for over 13 years. #halloween #facesofgarfield', 'facesofgarfield', 'Faces of Garfield', 637076057),
('579332161380845738_637076057', 1383281797, 'Garfield Historic District', 'http://distilleryimage2.s3.amazonaws.com/fce6eb0242b111e3938b22000a9f50cb_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gKM4gGEIiq/', 'Melanie, Wendy, Reyna, Brenda, & Karen, have lived in the #garfielddistrict for 8 years. They are out having fun on #halloween  #facesofgarfield', 'facesofgarfield', 'Faces of Garfield', 637076057),
('579352092403075629_637076057', 1383284173, 'Garfield Historic District', 'http://distilleryimage9.s3.amazonaws.com/8516a1fc42b711e3b54d22000ab5c4c5_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gKRaiTkIot/', 'Christian, 1 year old, his family has been living in the #garfielddistrict for over 13 years. #halloween #elmo #facesofgarfield', 'facesofgarfield', 'Faces of Garfield', 637076057),
('579355794731993710_637076057', 1383284614, 'Garfield Historic District', 'http://distilleryimage10.s3.amazonaws.com/8c27b96242b811e385d522000a9f3c76_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gKSQaXkIpu/', '#facesofgarfield :: Jose, 10 yrs old, family has lived in #garfielddistrict for 12 yrs. #halloween', 'facesofgarfield', 'Faces of Garfield', 637076057),
('587658650639698776_637076057', 1384274391, 'Garfield Historic District', 'http://distilleryimage0.s3.amazonaws.com/0d8c4b364bb911e3ba3612f3b041e1a7_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gnyG3gkItY/', '#facesofgarfield :: Beverly, 55 yrs old. In the #garfielddistrict for over 20 years. #neighborhood #bikes #streets #phoenix #arizona', 'facesofgarfield', 'Faces of Garfield', 637076057),
('587664053222018011_637076057', 1384275035, 'Garfield Historic District', 'http://distilleryimage7.s3.amazonaws.com/8d6c1c224bba11e3a9720e7aedef815c_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gnzVfDkIvb/', '#facesofgarfield :: Irving, 22 yrs old. Has lived in the #garfielddistrict for over 3 years. #sundayafternoon #streets #people #phoenix #arizona', 'facesofgarfield', 'Faces of Garfield', 637076057),
('587665694553835521_637076057', 1384275231, 'Garfield Historic District', 'http://distilleryimage9.s3.amazonaws.com/020c8b8e4bbb11e3ba25129d09c8e96a_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gnztXqkIgB/', '#facesofgarfield :: Kristapher, 18 yrs old. Moved to the #garfielddistrict a year ago. #sunlight #goldenhour #phoenix #arizona #evening', 'facesofgarfield', 'Faces of Garfield', 637076057),
('587675671284713659_637076057', 1384276420, 'Garfield Historic District', 'http://distilleryimage7.s3.amazonaws.com/c6efe1104bbd11e38ae512bc234f61c7_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gn1-jOEIi7/', '#facesofgarfield :: Lucio, age 55. #garfielddistrict resident for 30 yrs. #familyman #streets #evening #goldenhour #phoenix #arizona', 'facesofgarfield', 'Faces of Garfield', 637076057),
('587684111155497294_637076057', 1384277427, 'Garfield Historic District', 'http://distilleryimage6.s3.amazonaws.com/1e9a5fc44bc011e381f912649f8c7ba8_8.jpg', '', 'http://images.ak.instagram.com/profiles/profile_637076057_75sq_1383281005.jpg', 'http://instagram.com/p/gn35XdkIlO/', '#facesofgarfield :: Estevan, Genaro & Donovan; all 13yrs years of age. Growing up in #garfielddistrict #friends #neighbors #phoenixstreets #arizona #bikes', 'facesofgarfield', 'Faces of Garfield', 637076057);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
