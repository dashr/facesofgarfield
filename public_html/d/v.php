<?php

//simple session control
session_start();
if ( !isset($_SESSION['csrf']) &&  $_SESSION['csrf'] !== $_POST['csrf'] ) {
  return header('HTTP/1.0 401 Unauthorized');
  die();
}

//get configs
require '../../app/config.php';
require '../../app/instatag.php';

//vote
$i = isset($_POST['i']) ? (string)$_POST['i'] : '';
$v = isset($_POST['v']) ? (int)$_POST['v']    : 0;

echo Instatag::vote($i, $v);


/* Migration::Populate old Ones - Run Once on Launch
foreach ( Instatag::get_all('SELECT gram_id FROM media ORDER BY created_time ASC') as $i ) {
  global $dbh;
  $sql = sprintf("INSERT INTO votes SET gram_id='%s' ", $i['gram_id']);
  $dbh->query($sql);
}
*/
